<?php

class m210301_072707_create_group_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('Group', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'parent_id' => 'integer DEFAULT 0',
            'is_deleted' => 'boolean DEFAULT 0',
        ));
	}

	public function down()
	{
		echo "m210301_072707_create_group_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}