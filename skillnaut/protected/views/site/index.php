<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<?php if ($accessManageGroups) : ?>
    <div id="groups-hierarchy">
        <div class="inner">
            <?php echo $groups; ?>
        </div>
    </div>

    <script>
        $('document').ready(function () {
            $("#groups-hierarchy li span").draggable({
                containment: "#groups-hierarchy"
            });
        });
    </script>
<?php endif; ?>
